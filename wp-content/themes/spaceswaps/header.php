<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head(); ?>
		<?php
		$template_dir_url = get_template_directory_uri();		
		if(is_page(50))
		{
			echo '<script type="text/javascript" src="'.get_template_directory_uri().'/js/file_upload.js"></script>';
		}		
		if ( is_singular( 'listing' ) ) {
		?>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
		<?php } ?>
				
	</head>
	<body <?php body_class(); ?> data-spy="scroll" data-target=".navbar" data-offset="60" data-spy="affix">
	
	
<nav class="navbar navbar-expand-lg top-nav">
	<a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/image/logo.png"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	<nav class="navbar search-bx">
	<form class="form-inline" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input class="form-control mr-sm-2" id="search_field"  name="s" type="search" placeholder="Where do you want to stay?" aria-label="Search" autocomplete="off">
		</form>
	</nav>
	<div class="collapse navbar-collapse" id="navbarNav">
	<?php 
	if(is_user_logged_in())
	{ 
global $current_user;                     
$args = array(
    'post_type' => 'listing',
    'author' => $current_user->ID,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
$post_count = $query->post_count;
if($post_count==1)
{	
$p_id = $query->post->ID;
}
$user_id      = get_current_user_id();
$profile_img	= @json_decode(get_user_meta($user_id, 'profile_image', true));
$profile_img  = !$profile_img ? '' : $profile_img;
?>
		<ul id="menu-top-menu" class="navbar-nav">
			<li><a class="hitw" href="<?php echo home_url(); ?>/#howitwork">How it work</a></li>
			<?php 
			if($post_count==0)
			{
				echo '<li><a href="'.get_site_url().'/add-listing/">Add Listing</a></li>';
			}
			else
			{
				if(!is_page(90))
				{	
				echo '<li><a href="'.get_permalink(90).'/">Host Profile</a></li>';
				}
				echo '<li><a href="'.get_site_url().'/add-listing/">Edit Listing</a></li>';
			}		
			?>
			<li><?php echo do_shortcode('[xoo_el_action type="login" change_to="logout"]'); ?></li>
			<?php if($post_count==1)
			{ ?>
			<li class="top-pro-img"><a href="<?php echo get_permalink(127); ?>"><?php if($profile_img){ ?> <img class="rounded-circle" src="<?php echo $profile_img->thumb; ?>">
			<?php 
			}
			else
			{	
			?>	
			<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" class="rounded-circle" />
			<?php } ?>
			</a>
			</li>
			<?php } ?>
		</ul>
		
	<?php }
	else
	{
		 wp_nav_menu( array( 'menu' => 'Top Menu', 'menu_class' => 'navbar-nav' ) );
	}
	?>

	</div>
</nav>	