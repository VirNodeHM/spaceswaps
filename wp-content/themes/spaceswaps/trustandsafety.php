<?php 
/* Template Name: Trust And Safety */ 
get_header(); 
?>

	<main role="main" id="innerPage">
	<div class="container-fluid">
		<h1 class="title"><?php the_title(); ?></h1>
	</div>
	<div class="container innerPageContent trustsafetyPage" style="max-width:768px;">
		<?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php 
		$tab_content = get_field('tabs_content', $post->ID);
		?>
		
		<ul class="nav nav-tabs" role="tablist">
			<?php $i=1;  foreach($tab_content as $content) {?>
			<li class="nav-item">
				<a class="nav-link <?php if($i==1){ echo 'active'; } ?>" data-toggle="tab" href="#<?php echo $content['tab_title'] ?>" role="tab"><?php echo $content['tab_title'] ?></a>
			</li>
			<?php $i++; } ?>	
		</ul><!-- Tab panes -->
		
		<div class="tab-content">
			<?php $k=1; foreach($tab_content as $content) { ?>
			<div class="tab-pane <?php if($k==1){ echo 'active'; } ?>" id="<?php echo $content['tab_title'] ?>" role="tabpanel">
				<?php echo $content['tab_content'];  ?>
		     </div>
			<?php $k++; } ?>			
		</div>
		
		
		<?php endwhile; ?>
	</div>
		
	</main>
<?php get_footer(); ?>
