<?php get_header(); ?>
<?php
while (have_posts()) : the_post();
$p_id = $post->ID;
$page_object = get_page( $p_id );
$describe_pace = $page_object->post_content;;
$house_rules = get_field('house_rules', $p_id);
$other_content = get_field('other_content', $p_id);
$title = get_the_title($p_id);
$place_type = get_field('place_type', $p_id );
$what_will_guests_have = get_field('what_will_guests_have', $p_id );
$guests_accomodate = get_field('guests_accomodate', $p_id );
$guest_bedrooms = get_field('guest_bedrooms', $p_id );
$beds_use = get_field('beds_use', $p_id );
$gust_bathrooms = get_field('gust_bathrooms', $p_id );
$amenities = get_field('amenities', $p_id );
$check_out_request = get_field('check_out_request', $p_id);
$describe_yourself = get_field('describe_yourself',$p_id);
$dream_destinations = get_field('dream_destinations', $p_id);
$latitude = get_field('latitude', $p_id);
$longitude = get_field('longitude', $p_id);	
$not_avalabl = get_not_avalabl_dates($p_id);
$not_avalabl_dates = json_decode($not_avalabl->not_available_dates);
$attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_parent' => $p_id,
            'exclude'     => get_post_thumbnail_id()
        ) );
?>


	
	<script>
var citymap = {
        pinpoint: {
          center: {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>},
          population: 2714856
        }
      };

      function initMap() {
        // Create the map.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>},
          mapTypeId:google.maps.MapTypeId.ROADMAP
        });

        // Construct the circle for each value in citymap.
        // Note: We scale the area of the circle based on the population.
        for (var city in citymap) {
          // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#38C0B5',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#38C0B5',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
            radius: 3218
          });
        }
      }
	</script>
<style>
  #map {
    width: 100%;
    height: 500px;
    float: left;
  }
  </style>


<?php
	if($attachments)
	{
?>			
	<div class="container-fluid profile-slide mt-10">
		<div class="row">
			<div class="col-md-12">
			

    <!--Carousel Wrapper carousel-fade -->
    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
      <!--Slides-->
      <div class="carousel-inner" role="listbox">
        
		<?php 
		$i=1;
		foreach($attachments as $attachment)
		{
		?>
		<div class="carousel-item <?php if($i==1){ echo 'active'; } ?>">
          <img class="d-block w-100" src="<?php echo  $attachment->guid;?>" alt="First slide">
        </div>
		<?php $i++; } ?>
      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
      <ol class="carousel-indicators">
<?php 
		$k=0;
		foreach($attachments as $attachment)
		{
?>	  
        <li data-target="#carousel-thumb" data-slide-to="<?php echo $k; ?>" <?php if($k==0){ echo 'class="active"'; } ?> > 
		<img class="d-block w-100" src="<?php echo $attachment->guid; ?>" class="img-fluid">
		</li>
<?php $k++; } ?>      
	  </ol>
    </div> <!--/.Carousel Wrapper--> 
			</div><!-- col-md-12 End -->
		</div><!-- row end -->	
	</div><!-- container-fluid End -->
	

<?php 
	}
?>	
<div class="container-fluid pro-mid-sec">
<div class="row">
<div class="col-md-12 pro-mid-title">
	<h3><?php $title; ?> Ready for Swapping</h3>
</div>
<div class="col-md-12 pro-nav-list">
	<ul>
		<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/image/bedroom-icon.png" /><?php echo $guest_bedrooms;  ?> Bedrooms</a></li>
		<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/image/sleeps-icon.png" />Sleeps <?php echo $guests_accomodate;  ?></a></li>
		<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/image/bathroom-icon.png" /><?php echo $gust_bathrooms;  ?> Private Bathroom</a></li>
		<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/image/apartment-icon.png" /><?php ?><?php echo $place_type;  ?></a></li>
	</ul>
</div>
</div><!-- row End -->
<div class="row">
<div class="col-md-7 pro-left-sec">
<div class="our-space-sec">
	<h3>Our Space</h3>
	<?php echo $describe_pace; ?>
</div><!-- our-space End -->
<div class="our-availability-sec">
	<h3>Our Availability</h3>
	<ul>
		<li class="op-swap">Ready to Swap</li>
		<li class="sn-available">Sorry Not Available</li>
	</ul>
				<div class="container fluid-full">					
					<div class="calendar table-responsive">
<?Php
$year=2020; // change this to another year 2017 or  2018 or 2019 Or ...
if(strlen($year)!= 4){
$year=date('Y'); // Current Year is taken if year is not set in above line. 
}
$row=3;  //to set the number of rows and columns in yearly calendar ( 1 to 12 )
///// No Edit below //////
$row_no=0; // 
echo "<table class='main'>"; // Outer table 
////// Starting of for loop/// 
///  Creating calendars for each month by looping 12 times ///
$current_month = date('n');
$next_mont = $current_month+1;
for($m=$current_month;$m<=$next_mont;$m++){
$month =date($m);  // Month 
$dateObject = DateTime::createFromFormat('!m', $m);
$monthName = $dateObject->format('F'); // Month name to display at top

$d= 2; // To Finds today's date
//$no_of_days = date('t',mktime(0,0,0,$month,1,$year)); //This is to calculate number of days in a month
$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
//echo $j;// Sunday=0 , Saturday =6
//// if starting day of the week is Monday then add following two lines ///
$j=$j-1;  
if($j<0){$j=6;}  // if it is Sunday //
//// end of if starting day of the week is Monday ////


$adj=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$j);  // Blank starting cells of the calendar 
$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
if($blank_at_end >= 7){$blank_at_end = $blank_at_end - 7 ;} 
$adj2=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$blank_at_end); // Blank ending cells of the calendar

/// Starting of top line showing year and month to select ///////////////
if(($row_no % $row)== 0){
echo "</tr><tr>";
}

if($m==$next_mont)
{	
echo "<td><table class='main'><td colspan=8 align=center>$monthName $year <span current_list='".$p_id."' current_mn='".$next_mont."' class='next_mn'> > </span></td><td align='center'></td></tr>";
}
else
{
echo "<td><table class='main'><td colspan=8 align=center>$monthName $year</td><td align='center'></td></tr>";	
}	
echo "<tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr>";

for($i=1;$i<=$no_of_days;$i++){
$pv="$month"."/"."$i"."/"."$year";

if(in_array($pv, $not_avalabl_dates))
{
	echo $adj."<td class=\"date_td\"><a class=\"cal_a al_selected not_avlable\"  cur_date=\"$pv\">$i</a>";
}
else
{	
echo $adj."<td class=\"date_td\"><a class=\"cal_a\" cur_date=\"$pv\">$i</a>"; // This will display the date inside the calendar cell
}
echo " </td>";
$adj='';
$j ++;
if($j==7){echo "</tr><tr>"; // start a new row
$j=0;}

}
echo $adj2;   // Blank the balance cell of calendar at the end 

echo "</tr></table></td>";

$row_no=$row_no+1;
} // end of for loop for 12 months
echo "</table>";
?>
					
					</div>
				</div>	
	
	
</div><!-- our-availability-sec End -->
<div class="pro-details-sec">
	<h3>Profile</h3>
	<?php echo $describe_yourself; ?>
<div class="row mt-2">
	<div class="col-md-2">
	 <div class="profile-img">
	 <?php 
	 $user_id      = get_post_field( 'post_author', $p_id );
	 $profile_img	= @json_decode(get_user_meta($user_id, 'profile_image', true))	;
	 $profile_img  = !$profile_img ? '' : $profile_img;
	 if($profile_img){ 
		echo '<img class="rounded-circle" src="'.$profile_img->thumb.'">';
	 }
	 else
	 {
		echo '<img class="rounded-circle" src="'.get_template_directory_uri().'/image/default-profile-img.png">'; 
	 }	 
	 ?>
	 </div>
	</div>
	<div class="col-md-10">
		<h4>Dream Locations</h4>
		<ul>
			<?php foreach($dream_destinations as $place) { echo '<li>'.$place.'</li>'; } ?>
		</ul>
	</div>
</div>
</div><!-- pro-details-sec End -->


<div class="pro-mid-amenities">
<h3>Amenities</h3>
<?php 
if($amenities)
{
	echo '<ul>';
	foreach($amenities as $ameniti)
	{
	 if($ameniti=='Essentials (towels, bed sheets, soap, toilet parer and pillows)')	
	 {
		 $class = 'class="bed-icon"';
	 }		
	 if($ameniti=='Tv')	
	 {
		 $class = 'class="tv-icon"';
	 }
	 if($ameniti=='Wheelchair friendly')
	 {
		 $class = 'class="wheelchair-chair-icon"';
	 }	
	 if($ameniti=='Pool / Gym')
	 {
		 $class = 'class="pool-gym-icon"';
	 }	
	 if($ameniti=='Air-conditioning')
	 {
		 $class = 'class="air-conditioning-ph-icon"';
	 }	
	 if($ameniti=='Wifi')
	 {
		 $class = 'class="wifi-icon"';
	 }	
	 if($ameniti=='Breakfast')
	 {
		 $class = 'class="breakfast-icon"';
	 }	
	 if($ameniti=='Coffee and tea')
	 {
		 $class = 'class="coffee-and-tea-icon"';
	 }	
	 if($ameniti=='Desk / workspace')
	 {
		 $class = 'class="desk-and-workspace-icon"';
	 }	
	 if($ameniti=='Hair dryer')
	 {
		 $class = 'class="hair-dryer-icon"';
	 }
	 if($ameniti=='Laundry')
	 {
		 $class = 'class="laundry-icon"';
	 }
	 if($ameniti=='Private Entrance')
	 {
		 $class = 'class="private-entrance-icon"';
	 }
	 if($ameniti=='Shampoo')
	 {
		 $class = 'class="shampoo-icon"';
	 }	
	 if($ameniti=='Wheelchair friendly')
	 {
		 $class = 'class="wheelchair-chair-icon"';
	 }
	 if($ameniti=='Iron')
	 {
		 $class = 'class="iron-icon"';
	 }
	 if($ameniti=='Heating')
	 {
		 $class = 'class="heating-icon"';
	 }
	 if($ameniti=='Kitchen')
	 {
		 $class = 'class="kitchen-icon"';
	 }	 
	 
	 if($ameniti=='Essentials (towels, bed sheets, soap, toilet parer and pillows)')	
	 {
		 echo '<li '.$class.'>Essentials</li>';
	 }
	 else
	 {		
		echo '<li '.$class.'>'.$ameniti.'</li>';
	 }	
	}
	echo '</ul>';
}
?>	
</div><!-- pro-mid-amenities End -->




<?php if($check_out_request){
?>
<div class="pro-mid-thing">
	<h3>Things to Know</h3>

		<?php echo $check_out_request; ?>
</div><!-- pro-mid-thing End -->
<?php } ?>

<div class="pro-mid-location">
	<h3>Locations</h3>
	<div class="location-mp">
		<div id="map"></div>
	</div>
</div><!-- pro-mid-location End -->


<div class="pro-mid-review" style="clear: both;">
	<h3>Reviews</h3>								

	<div class="row">
	
	<div class="col-md-12 comment-text">
		<p>no reviews currently</p>
	</div>
	</div><!-- row end -->	

</div>

<?php
	$args = array(
	   'post_type' => 'listing',
	   'post__not_in' => array($p_id),
	   'meta_query' => array(
		  array(
			 'key' => 'country_name',
			 'value' => get_post_field('country_name', $p_id),
		  )		  
	   )
	 );
	$wp_query = new WP_Query($args);
	
//print_r($wp_query);	
	
if($wp_query->have_posts())
{?>
<!-- pro-mid-review End -->
<div class="pro-other-place">
	<h3>Other Similar Place Swaps</h3>	
	<div class="owl-carousel owl-theme">
	<?php 
	while( $wp_query->have_posts() ) : $wp_query->the_post();
	?>
		<div class="item">
			<div class="psw-bx">				
				<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg">
				<div class="over-text">
					<h3><a href="<?php echo get_permalink($wp_query->post->ID); ?>"><?php echo get_the_title($wp_query->post->ID); ?></a></h3>
					<?php 
					$post_author_id = get_post_field( 'post_author', $wp_query->post->ID );
					$profile_img	= @json_decode(get_user_meta($post_author_id, 'profile_image', true));
					$profile_img  = !$profile_img ? '' : $profile_img;
						 if($profile_img){ 
							echo '<img class="rounded-circle" src="'.$profile_img->thumb.'">';
						 }
						 else
						 {
							echo '<img class="rounded-circle" src="'.get_template_directory_uri().'/image/default-profile-img.png">'; 
						 }
						 ?>
					<div class="star-rt">
						<i class="fa fa-star"></i><span>4.8</span>
					</div>
				</div>								
			</div>
		</div><!-- owl-item End -->
	<?php endwhile; wp_reset_query(); wp_reset_postdata(); ?>	
</div><!-- owl-carousel End -->
</div><!-- pro-other-place End -->
<?php } 
else
{
	echo '<div class="pro-other-place">';
	 echo '<h3>Other Similar Place Swaps</h3>'; 
	 echo '<p>No Similar place found</p>';
	echo '</div>';
}
wp_reset_query();
wp_reset_postdata();	
?>
</div><!-- pro-left-sec End -->

<div class="col-md-5 pro-right-sec">
	<div class="col-md-12 check-in-frm">
		<form method="GET" action="<?php echo get_permalink(157) ?>">
			<div class="input-group input-daterange">
				<label>Date</label>
				<div class="input-group date">
					<input type="hidden" name="agent_id" value="<?php echo $user_id; ?>">
					<input type="text" class="form-control" placeholder="check-In"  id="datepicker" value="">		
					<div class="input-group-addon"><i class="fa fa-arrows-h" aria-hidden="true"></i></div>
					<input type="text" class="form-control" placeholder="checkout"  id="datepickersecond" value="">		
				</div>
			</div>
			<div class="form-group">
				<label>Guests</label>
				<div class="select-bx">
					<select>
					<?php
					for($i=1; $i<=$guests_accomodate; $i++)
					{
						echo '<option value="'.$i.'">'.$i.' guest</option>';
					}	
					?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<button class="btn btn-primary ">Swap Spaces</button>
			</div>
			<p>Start your adventure by swapping your space</p>
		</form>
		<p class="report-flag"><a href="#"><i class="fa fa-flag-o" aria-hidden="true"></i>Report this listing</a></p>
	</div>


</div><!-- pro-right-sec End -->
</div><!-- row End -->
</div><!-- pro-mid-sec End -->
<!-- Mid body End -->
<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3h7_i65CzE_FA6wVJHoBSY3sg9akiQ-g&callback=initMap">
</script>
<?php
$not_avalabl = get_not_avalabl_dates($p_id);
$not_avalabl_dates = json_decode($not_avalabl->not_available_dates);
$disable_date =[];
?>  
  <script>
  jQuery(document).ready(function(){
	 var disablethese = [
	 <?php
foreach($not_avalabl_dates as $not_avalabl)
{
 $newDate = date("Y-m-d", strtotime($not_avalabl));
 echo '"'.$newDate.'", ';	
}	 
?>
	 ]; 
	  
    jQuery( "#datepicker" ).datepicker({minDate: 0, beforeShowDay: function(date){
        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        return [ disablethese.indexOf(string) == -1 ]
    }});
	jQuery( "#datepickersecond" ).datepicker({minDate: 0, beforeShowDay: function(date){
        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
        return [ disablethese.indexOf(string) == -1 ]
    }});
  });  
  </script>



<?php endwhile; ?>
<?php get_footer(); ?>
