<?php /* Template Name: Host Profile Messages */?> 
<?php get_header(); 
global $current_user; 
$user_id      = get_current_user_id();
$profile_img	= @json_decode(get_user_meta($user_id, 'profile_image', true));
$profile_img  = !$profile_img ? '' : $profile_img;
$user_details = get_userdata($user_id);
$registered = $user_details->user_registered;
$user_messages = get_current_user_messages($user_id);

?>

<!-- pro-msg-sec start -->
<div class="container-fluid pro-msg-sec">
	<div class="row">
		<div class="col-md-7 pro-left-sec">
			<div class="row pro-name">
				<div class="col-md-12">
					<span>Hi</span><h4><?php echo $user_details->data->display_name; ?></h4>
				</div>
			</div>	
				
				<!-- Msg list -->
<?php 
if($user_messages)
{
	foreach($user_messages as $message)
	{
		$profile_img_sender	= @json_decode(get_user_meta($message->sender_id, 'profile_image', true));
		$profile_img_sender  = !$profile_img_sender ? '' : $profile_img_sender;
		$sender_details = get_userdata($message->sender_id);
		$message_date = $message->created_at;
		$dt = new DateTime($s);
		$date = $dt->format('m/d/Y');
		$args = array(
		'post_type' => 'listing',
		'author' => $message->sender_id,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
		);
		$query = new WP_Query($args);
		$p_id = $query->post->ID;
		$country_name = get_post_field('country_name', $p_id);
		$state_name = get_post_field('state_name', $p_id);
?>	
	<div class="row list-ms-rw">
		<ul class="msg-listing">
			<li class="msg-list-pic">
				<div class="p-img">
					<?php if($profile_img_sender){ ?> <img class="rounded-circle" src="<?php echo $profile_img_sender->thumb; ?>">
					<?php 
					}
					else
					{	
					?>	
					<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" class="rounded-circle" />
					<?php } ?>
				</div>
			</li>
			<li>
				<h4 class="log-name"><?php echo $user_details->data->display_name; ?></h4>
				<span class="log-date"><?php echo $date; ?></span>
			</li>
			<li class="log-msg">
				<p><?php echo $message->message; ?></P>
				<p><?php echo $country_name.', '.$state_name; ?></p>
			</li>
			</ul>
	</div>
	
	<?php } }
		else
		{?>
		<div class="row list-ms-rw">
		<ul class="msg-listing">
			<li>
				<p>There is no message</p>
			</li>	
</ul>
</div>			
		<?php
		}
		?>	
		
	<!-- Msg list end -->
				
			
		</div><!-- pro-left-sec end -->
		<div class="col-md-5 pro-right-sec">
			<div class="profile-view">
				<div class="row user-img">
					<div class="p-img">
					<?php
					if($profile_img)
					{ ?>
						<img src="<?php echo $profile_img->thumb; ?>" />
					<?php }
					else
					{	
					?>
						<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" />
					<?php } ?>	
					</div>
					<div class="user-ver-list">
						<ul>
							<li>Joined in <?php echo  date( "M Y", strtotime( $registered ) )  ?></li>
							<li>Change Photo</li>
						</ul>
					</div>
				</div>
				<ul>
					<li><a href="<?php echo get_permalink(90); ?>">View your listing</a></li>
					<li><a href="<?php echo get_permalink(127); ?>">Messages</a></li>
					<li><a href="#">Reviews</a></li>
				</ul>
			</div><!-- profile-view end -->
			
		</div><!-- pro-right-sec end -->
	</div>
</div><!-- pro-msg-sec end -->
<?php get_footer(); ?>
