<?php /* Template Name: Add Listing Third Step */?> 
<?php get_header(); 
if ( is_user_logged_in() ) {
global $current_user;                     

$args = array(
    'post_type' => 'listing',
    'author' => $current_user->ID,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
$post_count = $query->post_count;
if($post_count==1)
{
$p_id = $query->post->ID;
$page_object = get_page( $p_id );
$describe_pace = $page_object->post_content;;
$house_rules = get_field('house_rules', $p_id);
$other_content = get_field('other_content', $p_id);
$title = get_the_title($p_id);
$attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_parent' => $p_id,
            'exclude'     => get_post_thumbnail_id()
        ) );
	
}
?>  
<script type = "text/javascript">
$(document).ready(function() {

     $('.thrid_step').click(function(e){
		 $('.loader-outer').show();
        $('#listing_form_third_step').validate();
        if ($('#listing_form_third_step').valid()) // check if form is valid
        {
			 e.preventDefault(); 
			  var edit_post_id = $('#edit_post_id').val();		
			  var describe_pace = $('#describe_pace').val();
			  var house_rules = $('#house_rules').val();
			  var other_thing = $('#other_thing').val();
			  var listing_title = $('#listing_title').val();
			  var fd = new FormData();
			  var files_data = $('.files-data'); 
			  $.each($(files_data), function(i, obj) {
				$.each(obj.files,function(j,file){
					fd.append('files[' + j + ']', file);
				})
			  });
			  fd.append('action', 'cvf_upload_files');  
			  fd.append('post_id',edit_post_id);
				console.log(fd);
			  
			$.post(
				myAjax.ajaxurl, 
				{
				'action': 'update_listing_third_step',
				 data: {edit_post_id: edit_post_id, describe_pace : describe_pace, house_rules : house_rules,other_thing :other_thing, listing_title:listing_title}
				}, 
				function(response){
					var res = $.parseJSON(response);
						if(res.sucess == 'Yes')
						{	
						$.ajax({
							type: 'POST',
							url: myAjax.ajaxurl,
							data: fd,
							contentType: false,
							processData: false,
							success: function(response){
								location.href = "http://115.112.129.194:7171/spaceswaps/host-profile/";
							}
						});	
						}	
				}
			)  
        }
        else 
        {
            $('.loader-outer').hide();
        }
    });	




});                    
</script>
<!-- Mid body -->
<form class="form-horizontal" id="listing_form_third_step" name="form" method="post" enctype="multipart/form-data">
	<input type="hidden" name="post_id" value="<?php echo $p_id; ?>" id="edit_post_id">
	
<div class="container-fluid steps-link">
	<div class="row">
		<div class="col-md-4">
			<p class="stepTitle">Step 3</p>
		</div>
		<?php if(isset($p_id)) { ?>
		<div class="col-md-8 step-move-btn">
			<ul>
				<li><a href="<?php echo get_permalink(62); ?>">Step 2</a></li>
				<li><a href="<?php echo get_permalink(50); ?>">Step 1</a></li>
			</ul>
		</div>
		<?php } ?>
	
	</div>
</div>	
	<div class="container-fluid form-step-sec">
		<div class="row"> 
			
			<div class="col-md-12 step-nav">
				<nav class="navbar navbar-expand-sm" >  
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="nav-link" href="#Describe_yourplace">Describe your place</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#uploadphotos">Upload Photos</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#houserules">House rules</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#others">Others</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#naming">Naming</a>
					</li>					
				  </ul>
				</nav> 				
				
				
			</div>
			<div id="Describe_yourplace" class="col-md-12 step-details">
				<h3>Describe your place</h3>
				<p class="sub-content">Describe your place to guests (write a quick summery of your place. You can highlight what’s special about your space, the neighbourhood, and the experience your guests will have.</p>
				<textarea name="describe_pace" id="describe_pace" required><?php if(isset($describe_pace)){ echo $describe_pace; } ?></textarea>
			</div><!-- step-details End -->	
			<div id="uploadphotos" class="col-md-12 Photos-sec">
				<h3>Photos</h3>
				<p class="sub-content">Upload your photos. The more the better as it gives guests a more accurate and realistic expectation of your space. </p>	

			<div class="col-md-12 upload-img-bx upload-form">		
				<div class="file-area">	
					<input type="file" id="files" name="files[]" accept ="image/*" class="files-data form-control" multiple required="required"/>				
					<div class="file-dummy">
					  <div class="success"> + Upload</div>
					  <div class="default"> + Upload</div>
					</div>
				</div>
			<?php 
			if(isset($attachments))
			{
				foreach($attachments as $attachment)
				{
					echo '<span class="pip"><img class="imageThumb" src="'.$attachment->guid.'" title="undefined"><br><span class="remove remove_attachment" attachment_id="'.$attachment->ID.'">Remove</span></span>';
				}
			}				
			?>	
				
			</div>

				
			</div><!-- step-details End -->	
			<div id="houserules" class="col-md-12 house-rules-sec">
				<h3>House Rules</h3>
				<p class="sub-content">Space Swaps only works on a basis of trust and open communication. List your house rules below to let guests know what is allowed and the expectations of swapping their place with yours.</p>
				<textarea name="house_rules" id="house_rules" required><?php if(isset($house_rules)){echo $house_rules;} ?></textarea>
			</div><!-- house-rules-sec End -->
			<!--<div id="others" class="col-md-12 add-rules-sec">
				<h3>Want to add anything else?</h3>
				<p class="sub-content">Space Swaps only works on a basis of trust and open communication. List your house rules below to let guests know what is allowed and the expectations of swapping their place with yours.</p>
				<textarea name="other_thing" id="other_thing" required><?php if(isset($other_content)){echo $other_content;} ?></textarea>
			</div><!-- add-rules-sec End -->	
			<div id="naming" class="col-md-12 place-list-sec">
				<h3>Name your place (listing title)</h3>				
				<textarea name="listing_title" id="listing_title" required><?php if(isset($title)){ echo $title;} ?></textarea>
			</div><!-- place-list-sec End -->
			<div class="col-md-12 step-btn">			
				<a class="thrid_step" href="#">Save and Continue</a>
			</div>
		</div><!-- availability-step-sec End -->
		
	</div><!-- form-step-sec End -->
	
	
	
	
<!-- Mid body End -->




	
</form>	
 
  
<?php } else {
echo '<main role="main" id="innerPage">';
	echo '<div class="container-fluid">';
		echo '<h1 class="title">You Are not login</h1>';
	echo'</div>';
		echo '<div class="container innerPageContent" style="max-width:768px;">';	
		echo '<div class="alert alert-warning" role="alert">Click Here to login</div>';
		echo '</div>';
echo '</main>';
}
?>
<?php get_footer(); ?>
