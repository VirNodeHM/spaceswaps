<?php /* Template Name: Add Listing Second Step */?> 
<?php get_header(); 
if ( is_user_logged_in() ) {
global $current_user;                     
$args = array(
    'post_type' => 'listing',
    'author' => $current_user->ID,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
//print_r($query);
$post_count = $query->post_count;
if($post_count==1)
{
$p_id = $query->post->ID;		
$place_type = get_field('place_type', $p_id );
$what_will_guests_have = get_field('what_will_guests_have', $p_id );
$guests_accomodate = get_field('guests_accomodate', $p_id );
$guest_bedrooms = get_field('guest_bedrooms', $p_id );
$beds_use = get_field('beds_use', $p_id );
$gust_bathrooms = get_field('gust_bathrooms', $p_id );
$country_name = get_post_field('country_name', $p_id );
$state_name = get_post_field('state_name', $p_id );
$gust_bathrooms = get_field('gust_bathrooms', $p_id );
$amenities = get_field('amenities', $p_id );
if($amenities=='')
{
	$amenities =[];
}
$check_out_request = get_field('check_out_request', $p_id);
$latitude = get_field('latitude', $p_id);
$longitude = get_field('longitude', $p_id);	
}
else
{
$place_type = '';
$what_will_guests_have = '';
$guests_accomodate ='';
$guest_bedrooms = 1;
$beds_use = 1;
$gust_bathrooms = 1;
$amenities = [];
$check_out_request = '';
		
}
?>
    <script>
      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
		   <?php 
			 if(isset($longitude) && isset($latitude))
			 {
				//echo 'var latLng = new google.maps.LatLng('.$latitude.', '.$longitude.');';
				echo 'center: {lat: '.$latitude.', lng: '.$longitude.'},';
			 }
			else
			{
				echo 'center: {lat: -33.8688, lng: 151.2195},';
			}	
			 ?>	
          
          zoom: 10,
          mapTypeId: 'roadmap'
        });
		   <?php 
			 if(isset($longitude) && isset($latitude))
			 {	?>	
		   new google.maps.Circle({
            strokeColor: '#38C0B5',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#38C0B5',
            fillOpacity: 0.35,
            map: map,
            center: {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>},
            radius: 3218
          });
<?php 
			 }		  
?>		

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
		var circle =[];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
		  for(var i in circle) {
		circle[i].setMap(null);
	  }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];
		  	
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
var latitude = place.geometry.location.lat();
var longitude = place.geometry.location.lng();  

var filtered_array = place.address_components.filter(function(address_component){
    console.log(address_component.types);
	return address_component.types.includes("country");
	
}); 
	var country = filtered_array.length ? filtered_array[0].long_name: "";
    document.getElementById("latitude").value = latitude;
   document.getElementById("longitude").value = longitude;
   document.getElementById("country_name").value = country;
	 var circlelist = new google.maps.Circle({
				strokeColor: '#38C0B5',
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: '#38C0B5',
				fillOpacity: 0.35,
				map: map,
				center: place.geometry.location,
				radius: 2000
			  });			
			circle.push(circlelist);
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
		
		
      }

    </script>

  <style>
  #mapCanvas {
    width: 100%;
    height: 600px;
    float: left;
  }
  #infoPanel {
    float: left;
    margin-left: 10px;
  }
  #infoPanel div {
    margin-bottom: 5px;
  }
  </style>
<!-- Mid body -->
<form class="form-horizontal" id="listing_form_second" name="form" method="post" enctype="multipart/form-data">
<!-- Mid body -->
	<input type="hidden" name="post_id" value="<?php echo $p_id; ?>" id="edit_post_id">
	
	<div class="container-fluid steps-link">
	<div class="row">
		<div class="col-md-4">
			<p class="stepTitle">Step 2</p>
		</div>
		<?php if(isset($p_id)) { ?>
		<div class="col-md-8 step-move-btn">
			<ul>
				<li><a href="<?php echo get_permalink(50); ?>">Step 1</a></li>
				<li><a href="<?php echo get_permalink(74); ?>">Step 3</a></li>
			</ul>
		</div>
		<?php } ?>
	
	</div>
</div>
	
	<div class="container-fluid form-step-sec st-two">
		<div class="row"> 
			<div class="col-md-12 step-nav">
				<nav class="navbar navbar-expand-sm" >  
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="nav-link" href="#place_type">Place Type</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#place_description">Place Description</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#location">Location</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#amenities">Amenities</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#checkout">Check-out</a>
					</li>					
				  </ul>
				</nav> 					
			</div>			
			<div id="place_type" class="col-md-12 place-type-sec">
				<h3>Place Type</h3>
				<p class="sub-content">What kind of place are you swapping? First, let’s narrow things down. </p>
				<div class="form-label-group multi-check">
					<label class="destination-point">
					  <input type="radio" name="place_type"  value="Flat" <?php if($place_type== 'Flat'){echo "checked";} ?> required><span class="checkmark"><span>1.</span> Flat</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="place_type" value="House" <?php if($place_type== 'House'){echo "checked";} ?>><span class="checkmark"><span>2.</span> House</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="place_type"  value="Secondary Unit" <?php if($place_type== 'Secondary Unit'){echo "checked";} ?>><span class="checkmark"><span>3.</span> Secondary Unit</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="place_type" value="Bed and Breakfast"<?php if($place_type== 'Bed and Breakfast'){echo "checked";} ?>><span class="checkmark"><span>4.</span> Bed and Breakfast</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="place_type" value="Bedroom" <?php if($place_type== 'Bedroom'){echo "checked";} ?>><span class="checkmark"><span>5.</span> Bedroom</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="place_type" value="Unique Space" <?php if($place_type== 'Unique Space'){echo "checked";} ?>><span class="checkmark"><span>6.</span> Unique Space</span>
					</label>
				</div><!-- multi-check End -->
			</div><!-- place-type-sec End -->	
			<div id="place_description" class="col-md-12 guest-type-sec">
				<h3>What will guests have?</h3>
				<p class="sub-content">What kind of place are you swapping? First, let’s narrow things down.</p>
				<div class="form-label-group multi-check">
					<label class="destination-point">
					  <input type="radio" name="what_will_guests_have"  value="Entire Flat" required <?php if($what_will_guests_have=='Entire Flat'){echo "checked";} ?>><span class="checkmark"><span>1.</span> Entire Flat</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="what_will_guests_have" value="Private Room" <?php if($what_will_guests_have=='Private Room'){echo "checked";} ?>><span class="checkmark"><span>2.</span> Private Room</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="what_will_guests_have" value="Shared Room" <?php if($what_will_guests_have=='Shared Room'){echo "checked";} ?>><span class="checkmark"><span>3.</span> Shared Room</span>
					</label>
					<label class="destination-point">
					  <input type="radio" name="what_will_guests_have" value="Other" <?php if($what_will_guests_have=='Other'){echo "checked";} ?>><span class="checkmark"><span>4.</span> Other</span>
					</label>	
				</div><!-- multi-check End -->
			</div><!-- guest-type-sec End -->	
			<div class="col-md-12 guest-number-sec">
				<h3>How many guests can your place accomodate?</h3>
				<div class="count-sec">
					<span class="plus button">+</span>
					<input type="text" name="guests_accomodate" id="guests_accomodate" value="<?php if($guests_accomodate){echo $guests_accomodate;} else{echo "1";} ?>" maxlength="12"/>
					<span class="min button">-</span>
				</div><!-- count-sec End -->
			</div><!-- guest-number-sec End -->	
			<div class="col-md-12 guest-number-sec">
				<h3>How many bedrooms can guest use?</h3>
				<div class="count-sec">
					<span class="plus button">+</span>
					<input type="text" name="guest_bedrooms" id="guest_bedrooms"  value="<?php if($guest_bedrooms){echo $guest_bedrooms;} else{echo "1";} ?>"maxlength="12"/>
					<span class="min button">-</span>
				</div><!-- count-sec End -->
			</div><!-- guest-number-sec End -->	
			<div class="col-md-12 guest-number-sec">
				<h3>How many beds can guest use?</h3>
				<div class="count-sec">
					<span class="plus button">+</span>
					<input type="text" name="beds_use" id="beds_use" value="<?php if($beds_use){echo $beds_use;} else{echo "1";} ?>" maxlength="12"/>
					<span class="min button">-</span>
				</div><!-- count-sec End -->
			</div><!-- guest-number-sec End -->	
			<div class="col-md-12 guest-number-sec">
				<h3>How bathrooms can guest use?</h3>
				<div class="count-sec">
					<span class="plus button">+</span>
					<input type="text" name="gust_bathrooms" id="gust_bathrooms" value="<?php if($gust_bathrooms){echo $gust_bathrooms;} else{echo "1";} ?>" maxlength="12"/>
					<span class="min button">-</span>
				</div><!-- count-sec End -->
			</div><!-- guest-number-sec End -->	
			<div id="location" class="col-md-12 step-details">
				<h3>Location?</h3>
				<p class="sub-content">This will not displayed on your preofile, only an approximate. Oly confirmed guest will see this so they know how to get to your place. </p>
				<div class="locatin-map">
				<input id="pac-input" class="controls" type="text" value="" placeholder="Search Address">	
<div id="mapCanvas"></div>
  <div id="infoPanel">
    <div id="markerStatus" style="opacity:0"><i>Click and drag the marker.</i></div>
    <div id="info"style="opacity:0"></div>
    <div id="address" style="opacity:0"></div>
	<input type="hidden" name="latitude" id="latitude" value="<?php if($latitude) { echo $latitude; } ?>">
	<input type="hidden" name="longitude" id="longitude" value="<?php if($longitude) { echo $longitude; } ?>">
	<input type="hidden" name="country_name" id="country_name" value="<?php if($country_name) { echo $country_name; } ?>">
	<input type="hidden" name="state_name" id="state_name" value="<?php if($state_name) { echo $state_name; } ?>">
  </div>
</div>
			</div><!-- Location section End -->
			<div id="amenities" class="col-md-12 amenities-detail-sec">
				<h3>Amenities?</h3>
				<p class="sub-content">What will guests have access to? </p>
				<div class="form-label-group multi-check">
					<label class="destination-point">
					  <input type="checkbox" name="amenities" value="Essentials (towels, bed sheets, soap, toilet parer and pillows)" <?php if(in_array('Essentials (towels, bed sheets, soap, toilet parer and pillows)',$amenities)){echo "checked";} ?>><span class="checkmark">Essentials (towels, bed sheets, soap, toilet parer and pillows)</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Private Entrance" required <?php if(in_array('Private Entrance',$amenities)){echo "checked";} ?>><span class="checkmark">Private Entrance</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Air-conditioning" <?php if(in_array('Air-conditioning',$amenities)){echo "checked";} ?>><span class="checkmark">Air-conditioning</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Shampoo" <?php if(in_array('Shampoo',$amenities)){echo "checked";} ?>><span class="checkmark">Shampoo</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Heating" <?php if(in_array('Heating',$amenities)){echo "checked";} ?>><span class="checkmark">Heating</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Wifi" <?php if(in_array('Wifi',$amenities)){echo "checked";} ?>><span class="checkmark">Wifi</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox" name="amenities" value="Hair dryer" <?php if(in_array('Hair dryer',$amenities)){echo "checked";} ?>><span class="checkmark">Hair dryer</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Desk / workspace" <?php if(in_array('Desk / workspace',$amenities)){echo "checked";} ?>><span class="checkmark">Desk / workspace</span>
					</label>
					
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Iron" <?php if(in_array('Iron',$amenities)){echo "checked";} ?>><span class="checkmark">Iron</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Coffee and tea" <?php if(in_array('Coffee and tea',$amenities)){echo "checked";} ?>><span class="checkmark">Coffee and tea</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Tv" <?php if(in_array('Tv',$amenities)){echo "checked";} ?>><span class="checkmark">Tv</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Breakfast" <?php if(in_array('Breakfast',$amenities)){echo "checked";} ?>><span class="checkmark">Breakfast</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Kitchen" <?php if(in_array('Kitchen',$amenities)){echo "checked";} ?>><span class="checkmark">Kitchen</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Pool / Gym" <?php if(in_array('Pool / Gym',$amenities)){echo "checked";} ?>><span class="checkmark">Pool / Gym</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Laundry" <?php if(in_array('Laundry',$amenities)){echo "checked";} ?>><span class="checkmark">Laundry</span>
					</label>
					<label class="destination-point">
					  <input type="checkbox"  name="amenities" value="Wheelchair friendly" <?php if(in_array('Wheelchair friendly',$amenities)){echo "checked";} ?>><span class="checkmark">Wheelchair friendly</span>
					</label>
				</div><!-- multi-check End -->				
			</div><!-- Amenities Section End -->
			<div id="checkout" class="col-md-12 step-details">
				<h3>Check-out request?</h3>
				<p class="sub-content">List any check-out requests here. This could range from feeding the fish to key drop-off.</p>
				<textarea name="check_out_request" id="check_out_request" required><?php if($check_out_request){echo $check_out_request;} ?></textarea>
			</div><!-- Amenities Section End -->
			<div class="col-md-12 step-btn">			
				<a href="#" class="second_step">Save and Continue</a>
			</div><!-- Step btn End -->
		</div><!-- row End -->
	</div><!-- form-step-sec End -->
<!-- Mid body End -->
</form>	
 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCapIdD5Q61vBuSQ4OWg9yzWDuVX6CvenI&libraries=places&callback=initAutocomplete"
         async defer></script>
  
<?php } else {
echo '<main role="main" id="innerPage">';
	echo '<div class="container-fluid">';
		echo '<h1 class="title">You Are not login</h1>';
	echo'</div>';
		echo '<div class="container innerPageContent" style="max-width:768px;">';	
		echo '<div class="alert alert-warning" role="alert">Click Here to login</div>';
		echo '</div>';
echo '</main>';
}
?>
<?php get_footer(); ?>
