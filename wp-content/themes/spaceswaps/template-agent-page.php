<?php /* Template Name: Agent Page */?> 
<?php get_header(); 
global $current_user;
$user_id      = $_GET['agent_id'];
$profile_img_reciver	= @json_decode(get_user_meta($user_id, 'profile_image', true));
$profile_img_reciver  = !$profile_img_reciver ? '' : $profile_img_reciver;
$user_details = get_userdata($user_id);
$login_user_id = $current_user->ID;
$profile_img_sender	= @json_decode(get_user_meta($login_user_id, 'profile_image', true));
$profile_img_sender  = !$profile_img_sender ? '' : $profile_img_sender;
$prev_messages = get_user_prev_message($login_user_id,$user_id);
?>

<!-- pro-msg-sec start -->
<div class="container-fluid pro-msg-sec">
	<div class="row">
		<div class="col-md-7 pro-left-sec">
			<div class="row pro-name">
				<div class="col-md-12">
					<span>Hi</span><h4><?php echo $user_details->data->display_name; ?></h4>
				</div>
			</div>
			<div class="row msg-rw">
				<div class="col-md-10 write-msg-bx">
				<form class="form-horizontal" id="send_message_form" name="form" method="post">
					<textarea placeholder="Write message here...." name="message_body" id="message_body"></textarea>
					<input type="hidden" name="sender_id" class="sender_id" value="<?php echo $login_user_id;?>">
					<input type="hidden" name="reciver_id" class="reciver_id" value="<?php echo $user_id;?>">
					<button class="btn sub-btn send_message_button">Send Message</button>
				</form>
				</div>
				<div class="col-md-2 msg-profile">
						<div class="p-img">
							<?php if($profile_img_reciver){ ?> <img class="rounded-circle" src="<?php echo $profile_img_reciver->thumb; ?>">
							<?php 
							}
							else
							{	
							?>	
							<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" class="rounded-circle" />
							<?php } ?>
						</div>
				</div>
			</div><!-- write-msg-bx end -->
<?php if($prev_messages){ 
foreach($prev_messages as $message)
{
?>			
	<div class="row msg-rw show-msg">
		<div class="col-md-10 write-msg-bx">
			<p> <?php echo $message->message; ?></p>
			<span class="date-show"></span>
		</div>
		<div class="col-md-2 msg-profile">
			<div class="p-img">
				<?php if($profile_img_sender){ ?> <img class="rounded-circle" src="<?php echo $profile_img_sender->thumb; ?>">
				<?php 
				}
				else
				{	
				?>	
				<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" class="rounded-circle" />
				<?php } ?>
			</div>
		</div>
	</div><!-- show-msg end -->
	
<?php 
}
} 
?>			
		</div><!-- pro-left-sec end -->
		<div class="col-md-5 pro-right-sec">
			<div class="profile-view">
				<div class="row user-img">
					<div class="p-img">
					<?php
					if($profile_img_sender)
					{ ?>
						<img src="<?php echo $profile_img_sender->thumb; ?>" />
					<?php }
					else
					{	
					?>
						<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" />
					<?php } ?>	
					</div>
					<div class="user-ver-list">
						<ul>
							<li>Joined in 2020</li>
							<li>Change Photo</li>
						</ul>
					</div>
				</div>
				<ul>
					<li><a href="<?php echo get_permalink(90); ?>">View your listing</a></li>
					<li><a href="<?php echo get_permalink(127); ?>">Messages</a></li>
					<li><a href="#">Reviews</a></li>
					<li><a href="#">Verify Account</a></li>
				</ul>
			</div><!-- profile-view end -->
			
		</div><!-- pro-right-sec end -->
	</div>
</div><!-- pro-msg-sec end -->


<!--  Modal Container Start -->
<div class="container">
<!-- Modal -->
	<div class="modal fade" id="message_sent_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" data-backdrop="static" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			Your message sent successfully. 
		  </div>
			<span class="close_pop_up">Ok</span>
		</div>
	  </div>
	</div><!--  Modal End -->	
</div><!--  Modal Container End -->



<?php get_footer(); ?>
