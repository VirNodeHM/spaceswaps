<?php /* Template Name: Add Listing Page */?> 
<?php get_header(); 
if ( is_user_logged_in() ) {
global $current_user;
$args = array(
    'post_type' => 'listing',
    'author' => $current_user->ID,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
$post_count = $query->post_count;
$not_avalabl_dates=[];
$dream_destinations=[];
if($post_count==1)
{
$p_id = $query->post->ID;	
$describe_yourself = get_field('describe_yourself',$p_id);
$dream_destinations = get_field('dream_destinations', $p_id);
$not_avalabl = get_not_avalabl_dates($p_id);
$not_avalabl_dates = json_decode($not_avalabl->not_available_dates);
}	
	
?>  
<!-- Mid body -->
<form class="form-horizontal" id="listing_form" name="form" method="post" enctype="multipart/form-data">

<div class="container-fluid steps-link">
	<div class="row">
		<div class="col-md-4">
			<p class="stepTitle">Step 1</p>
		</div>
		<?php if(isset($p_id)) { ?>
		<div class="col-md-8 step-move-btn">
			<ul>
				<li><a href="<?php echo get_permalink(62); ?>">Step 2</a></li>
				<li><a href="<?php echo get_permalink(74); ?>">Step 3</a></li>
			</ul>
		</div>
		<?php } ?>
	
	</div>
</div>

	<div class="container-fluid form-step-sec firststep">
		<div class="row"> 		
			<div class="col-md-12 step-nav">			
				<nav class="navbar navbar-expand-sm" >  
				  <ul class="navbar-nav">
					<li class="nav-item">
					  <a class="nav-link" href="#sd-sec">Describe</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#dream-ds-sec">Yourself</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="#availability-sp-sec">Availability</a>
					</li>					
				  </ul>
				</nav> 
			</div>
<?php 			
$user_id      = get_current_user_id();
$profile_img	= @json_decode(get_user_meta($user_id, 'profile_image', true));
$profile_img  = !$profile_img ? '' : $profile_img;
if($profile_img)
{	
?>
			<div class="add-profile-img user_image_uploded">
			<img class="uploaded_img" src="<?php echo $profile_img->thumb; ?>">
			<input data-type="image" type="file" data-ajaxed="Y"  name="image" class="upload" />
			<label>Add Profile image</label>
			</div>
<?php } 
else
{ ?>
			<div class="add-profile-img">
			<img class="uploaded_img" src="">
			<input data-type="image" type="file" data-ajaxed="Y"  name="image" class="upload" />
			<label>Add Profile image</label>
			</div>	
<?php }
?>			
			<div id="sd-sec"  class="col-md-12 step-details">
				<h3>Tell us about yourself</h3>
				<p class="sub-content">Describe yourself, what you like and dislike, why you love to travel and special quirks. This will help other swappers know who they are swapping with.</p>
				<textarea id="describe_yourself" name="describe_yourself" required><?php if(isset($describe_yourself)){echo $describe_yourself; } ?></textarea>
			</div>
			<div id="dream-ds-sec" class="col-md-12 dream-destinations-sec">
				<h3>Dream destinations</h3>
				<p class="sub-content">Choose your top 8 dream destinations. This will highlight to other swappers where you want to travel and swap.</p>
				<div class="form-label-group multi-check">
					<label class="destination-point des-place-int autocomplete">
					  <input type="text" id="myInput1" name="dream_destination" placeholder="Enter Here" value="<?php if (array_key_exists(0,$dream_destinations)) { echo $dream_destinations[0]; } ?>"<?php if (array_key_exists(0,$dream_destinations)) { echo 'class="orange-bg"'; } ?>>
					</label>
					<label class="destination-point des-place-int autocomplete">
					  <input type="text" id="myInput2" name="dream_destination" placeholder="Enter Here" value="<?php if (array_key_exists(1,$dream_destinations)) { echo $dream_destinations[1]; } ?>"<?php if (array_key_exists(1,$dream_destinations)) { echo 'class="orange-bg"'; } ?>>
					</label>
					<label class="destination-point des-place-int autocomplete">
					  <input type="text" id="myInput3" name="dream_destination" placeholder="Enter Here" value="<?php if (array_key_exists(2,$dream_destinations)) { echo $dream_destinations[2]; } ?>" <?php if (array_key_exists(2,$dream_destinations)) { echo 'class="orange-bg"'; } ?>>
					</label>
					<label class="destination-point des-place-int autocomplete">
					  <input type="text" id="myInput4" name="dream_destination" placeholder="Enter Here" value="<?php if (array_key_exists(3,$dream_destinations)) { echo $dream_destinations[3]; } ?>" <?php if (array_key_exists(3,$dream_destinations)) { echo 'class="orange-bg"'; } ?>>
					</label>					
				</div><!-- multi-check End -->
			</div><!-- dream-destinations-sec End -->
			<div id="availability-sp-sec" class="col-md-12 availability-step-sec calendar-dgn">
				<h3>Availability</h3>
				<p class="sub-content">Choose your availability for swapping. As we know, this will keep changing as things pop up with work, friends and family, and life in general but being specific can make future swaps easier. </p>
				<!-- Calendar Box -->
				<ul>
						<li class="rd-swap"><input type="radio" id="ready-to-swap" name="day_type" value="read_swap">
							<label for="ready-to-swap">Ready to Swap</label>
						</li>
						<li class="sn-available">
							<input type="radio" id="sorry-not-available" name="day_type" value="not_avlable">
  							<label for="sorry-not-available">Sorry Not Available</label>							
						</li>
					</ul>

<div class="not_avlable_div" style="opacity:0; display:none;">
<?php 
if($not_avalabl_dates)
{
	foreach($not_avalabl_dates as $dates)
	{
		echo '<input type="text" class="'.str_replace('/','',$dates).'" name="not_avalabl" value="'.$dates.'">';
	}
}	
?>
</div>					
					
					
				<div class="container fluid-full">					
					<div class="calendar table-responsive">
<?Php
$year=2020; // change this to another year 2017 or  2018 or 2019 Or ...
if(strlen($year)!= 4){
$year=date('Y'); // Current Year is taken if year is not set in above line. 
}


$row=3;  //to set the number of rows and columns in yearly calendar ( 1 to 12 )
///// No Edit below //////
$row_no=0; // 
echo "<table class='main'>"; // Outer table 
////// Starting of for loop/// 
///  Creating calendars for each month by looping 12 times ///
for($m=1;$m<=12;$m++){
$month =date($m);  // Month 
$dateObject = DateTime::createFromFormat('!m', $m);
$monthName = $dateObject->format('F'); // Month name to display at top



$d= 2; // To Finds today's date
//$no_of_days = date('t',mktime(0,0,0,$month,1,$year)); //This is to calculate number of days in a month
$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
//echo $j;// Sunday=0 , Saturday =6
//// if starting day of the week is Monday then add following two lines ///
$j=$j-1;  
if($j<0){$j=6;}  // if it is Sunday //
//// end of if starting day of the week is Monday ////


$adj=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$j);  // Blank starting cells of the calendar 
$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
if($blank_at_end >= 7){$blank_at_end = $blank_at_end - 7 ;} 
$adj2=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$blank_at_end); // Blank ending cells of the calendar

/// Starting of top line showing year and month to select ///////////////
if(($row_no % $row)== 0){
echo "</tr><tr>";
}
echo "<td><table class='main' ><td colspan=8 align=center> $monthName $year <a class='unavilable_action'  mn_days=\"$no_of_days\">Month not available</a></td><td align='center'></td></tr>";
echo "<tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr>";

for($i=1;$i<=$no_of_days;$i++){
$pv="$month"."/"."$i"."/"."$year";

if(in_array($pv, $not_avalabl_dates))
{
	echo $adj."<td class=\"date_td\"><a class=\"cal_a al_selected not_avlable\" cur_date=\"$pv\">$i</a>";
}
else
{	
echo $adj."<td class=\"date_td\"><a class=\"cal_a\" cur_date=\"$pv\">$i</a>"; // This will display the date inside the calendar cell
}
echo " </td>";
$adj='';
$j ++;
if($j==7){echo "</tr><tr>"; // start a new row
$j=0;}

}
echo $adj2;   // Blank the balance cell of calendar at the end 

echo "</tr></table></td>";

$row_no=$row_no+1;
} // end of for loop for 12 months
echo "</table>";
?>
					
					</div>
				</div>
				<!-- Calendar Box End -->
			</div><!-- row End -->
			<div class="col-md-12 step-btn">			
				<a class="first_step" href="#">Save and Continue</a>
			</div>
		</div><!-- availability-step-sec End -->	
	</div><!-- form-step-sec End -->
	
</form>	

  
<?php } else {
echo '<main role="main" id="innerPage">';
	echo '<div class="container-fluid">';
		echo '<h1 class="title">You Are not login</h1>';
	echo'</div>';
		echo '<div class="container innerPageContent" style="max-width:768px;">';	
		echo '<div class="alert alert-warning" role="alert">Click Here to login</div>';
		echo '</div>';
echo '</main>';
}
?>

<!--  Modal Container Start -->
<div class="container">
<!-- Modal -->
	<div class="modal fade" id="upload_image_validation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" data-backdrop="static" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			Image size must not exceed 2mb. 
		  </div>
			<span class="close_pop_up">Ok</span>
		</div>
	  </div>
	</div><!--  Modal End -->	
</div><!--  Modal Container End -->

<?php get_footer(); ?>
