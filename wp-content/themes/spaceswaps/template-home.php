<?php /* Template Name: Home Page Template */ 
get_header(); ?>

	<div class="container-fluid outer-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Chase your adventure... not your travel funds</h3>
				<p>Swap your place with other travellers</p>
			</div>
			<div class="col-md-6 sw-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg" />
					<div class="over-text">
						<a href="http://115.112.129.194:7171/spaceswaps/?s=">
						<h3>Swap your space</h3>
						<p>Find others to swap places</p>
						</a>
					</div>
								
			</div>
			<div class="col-md-6 sw-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-2.jpg" />
					<div class="over-text">
						<a class="slide_a <?php if(!is_user_logged_in()){ echo "xoo-el-reg-tgr";} ?>">
						<h3>Create your profile</h3>
						<p>Get started and create your unique profile</p>
						</a>
					</div>
									
			</div>
		</div>		
	</div>
	</div>
	<div class="container-fluid psw-sec">
		<div class="container">
			<div class="col-md-12">
				<h3>Places to stay around the world</h3>				
			</div>
		</div>
	<div class="row">
			<div class="col-md-4 psw-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg" />
					<div class="over-text">
						<h3>Swap your space</h3>
						<p>Find others to swap places</p>
						<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg" />
						<div class="star-rt">
							<i class="fa fa-star"></i><span>4.8</span>
						</div>
					</div>								
			</div>
			<div class="col-md-4 psw-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-2.jpg" />
					<div class="over-text">
						<h3>Create your profile</h3>
						<p>Get started and create your unique profile</p>
						<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg" />
						<div class="star-rt">
							<i class="fa fa-star"></i><span>4.8</span>
						</div>
					</div>								
			</div>
			<div class="col-md-4 psw-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-2.jpg" />
					<div class="over-text">
						<h3>Create your profile</h3>
						<p>Get started and create your unique profile</p>
						<img src="<?php echo get_template_directory_uri(); ?>/image/demo-img-1.jpg" />
						<div class="star-rt">
							<i class="fa fa-star"></i><span>4.8</span>
						</div>
					</div>								
			</div>	
		</div>
	</div>
	




	<!-- testimonial -->
<div class="container-fluid hm-step" id="howitwork"> 
	<div class="container">
		<div class="row">
			<h3>How it Works</h3>
		</div> 
	</div>
  <div id="quote-carousel" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#quote-carousel" data-slide-to="1"></li>
    <li data-target="#quote-carousel" data-slide-to="2"></li>
    <li data-target="#quote-carousel" data-slide-to="3"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
		 <blockquote>
				  <div class="row">					
						<div class="col-md-7 text-tss">
						   <h4>Step-1</h4>
						   <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>						  
						</div>
						<div class="col-md-5 profile-tss">
						  <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/image/man-img.png">
						</div>
				  </div>
		</blockquote>
    </div>
    <div class="carousel-item">
      <blockquote>
              <div class="row">
					<div class="col-md-7 text-tss">
						<h4>Step-2</h4>
					    <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
					</div>
					<div class="col-md-5 profile-tss">
					  <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/image/man-img.png">
					</div>
              </div>
            </blockquote>
    </div>
    <div class="carousel-item">
      <blockquote>
              <div class="row">                
                <div class="col-md-7 text-tss">
                   <h4>Step-3</h4>
				   <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                </div>
				<div class="col-md-5 profile-tss">
                  <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/image/man-img.png">
                </div>
              </div>
            </blockquote>
    </div>
	<div class="carousel-item">
		<blockquote>
              <div class="row">                
                <div class="col-md-7 text-tss">
                   <h4>Step-4</h4>
				   <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                </div>
				<div class="col-md-5 profile-tss">
                  <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/image/man-img.png">
                </div>
              </div>
		</blockquote>
    </div>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#quote-carousel" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#quote-carousel" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>   
  
</div>

	<!-- testimonial End -->





	
	<div class="container tss">
		<div class="row">
			<div class="col-md-12">
				<h3>Travelling with Space Swaps</h3>				
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-1.png" />					
					<h3>Global hospitality standards</h3>
					<p>Guests review their hosts after each stay. All hosts must maintain a minimum rating and our hospitality standards to be on Space Swaps.</p>						
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-2.png" />					
					<h3>Customer support</h3>
					<p>Talk to our support team from anywhere in the world, any time of the day. Send us an email to get in contact.</p>
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-3.png" />					
					<h3>Unique experience</h3>
					<p>Stay at a locals place not just another hotel room.</p>
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-4.png" />					
					<h3>Making friends</h3>
					<p>Guests review their hosts after each stay. All hosts must maintain a minimum rating and our hospitality standards to be on Space Swaps.</p>						
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-5.png" />					
					<h3>Wanderlust travellers on a budget</h3>
					<p>Day or night, we’re here for you. Talk to our support team from anywhere in the world, any time of the day.</p>
			</div>
			<div class="col-md-4 tss-bx">				
					<img src="<?php echo get_template_directory_uri(); ?>/image/demo-icon-6.png" />					
					<h3>Safe way to travel</h3>
					<p>Day or night, we’re here for you. Talk to our support team from anywhere in the world, any time of the day.
</p>
			</div>
		</div>		
	</div>
<!-- Mid body End -->

	
	
<?php get_footer(); ?>
