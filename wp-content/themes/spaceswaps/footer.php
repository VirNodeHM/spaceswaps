			<!-- footer section -->
			<footer class="footer-outer mt-10">
				<div class="container">
					<div class="row">
						<div class="col-md-10 copy-write">
						<p><img src="<?php echo get_template_directory_uri(); ?>/image/footer-logo.png" />2020 Space Swaps. All rights reserved.</p>
						</div>
						<div class="col-md-2 footer-link">
							<div class="social-link">
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="https://www.instagram.com/spaceswaps/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a>
							</div>
							<?php wp_nav_menu( array( 'menu' => 'Footer Menu'  ) ) ?>
						</div>
					</div>
				</div>
			</div>
			<!-- footer section End -->

<div class="loader-outer">        
	<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
   </div>			
			<?php 
			echo '<script type="text/javascript" src="'.get_template_directory_uri().'/js/autocomplete.js"></script>';
				echo '<script>autocomplete(document.getElementById("search_field"), countries);</script>';
			if(is_page(50))
			{ ?>
				<script>
				autocomplete(document.getElementById("myInput1"), countries);
				autocomplete(document.getElementById("myInput2"), countries);
				autocomplete(document.getElementById("myInput3"), countries);
				autocomplete(document.getElementById("myInput4"), countries);
				autocomplete(document.getElementById("myInput5"), countries);
				autocomplete(document.getElementById("myInput6"), countries);
				autocomplete(document.getElementById("myInput7"), countries);
				autocomplete(document.getElementById("myInput8"), countries);
				</script>
			<?php }
			wp_footer(); 
			?>		
	</body>
</html>
