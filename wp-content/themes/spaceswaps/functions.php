<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('jQuery', get_template_directory_uri() . '/js/jquery.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('jQuery'); // Enqueue it!
    	wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('bootstrap'); // Enqueue it!

    	wp_register_script('owlcarousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('owlcarousel'); // Enqueue it!				
		
    	wp_register_script('jqvalidation', get_template_directory_uri() . '/js/jquery.validate.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('jqvalidation'); // Enqueue it!		
    	wp_register_script('sitejs', get_template_directory_uri() . '/js/site-js.js', array(), '4.3.0'); // Conditionizr
		wp_localize_script( 'sitejs', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ))); 
		wp_enqueue_script( 'sitejs' );
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page(90)) {
        wp_register_script('sliderscript', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array(), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('sliderscript'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!
	
	wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '1.0', 'all');
    wp_enqueue_style('fontawesome'); // Enqueue it!

    wp_register_style('carouselcss', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1.0', 'all');
    wp_enqueue_style('carouselcss'); // Enqueue it!


    wp_register_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0', 'all');
    wp_enqueue_style('bootstrapcss'); // Enqueue it!



    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}


add_action( 'wp_ajax_nopriv_save_listing_first_step', 'save_listing_first_step' );
add_action( 'wp_ajax_save_listing_first_step', 'save_listing_first_step' );

function save_listing_first_step() {
global $current_user;
global $wpdb;
$availability_calendar_table = $wpdb->prefix . "availability_calendar";
get_currentuserinfo();
$describe_yourself = $_REQUEST['data']['describe_yourself'];
$dream_destinations =$_REQUEST['data']['dream_destinations'];
$not_avalabl =$_REQUEST['data']['not_avalabl'];
$user_login = $current_user->user_login;
$user_email = $current_user->user_email;
$user_firstname = $current_user->user_firstname;
$user_lastname = $current_user->user_lastname;
$user_id = $current_user->ID;
$post_title ='';
$post_content ='';
$args = array(
    'post_type' => 'listing',
    'author' => $user_id,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
//print_r($query);
$post_count = $query->post_count;
if($post_count==1)
{
$p_id = $query->post->ID;	
$ID = $wpdb->get_results("SELECT `ID` FROM `".$availability_calendar_table."` WHERE (listing_id = '". $p_id ."')");	
if($ID)
{
	$fld_id = $ID[0]->ID;
	$not_avalabl_dates = json_encode($not_avalabl);
	 $wpdb->query( $wpdb->prepare("UPDATE $availability_calendar_table 
                SET not_available_dates = %s 
             WHERE ID = %d",$not_avalabl_dates, $fld_id)
			 
			 
    );	
}
else{
	$not_avalabl_dates = json_encode($not_avalabl);
	$wpdb->insert($availability_calendar_table, array(
                                'user_id' => $user_id, 
                                'not_available_dates' => $not_avalabl_dates,
								'listing_id' => $p_id,
                                ),array(
                                '%d',
                                '%s',
								'%d') 
        );		
}	


update_post_meta($p_id, 'dream_destinations', $dream_destinations);
update_post_meta($p_id, 'describe_yourself', $describe_yourself);
$data['sucess'] ='Yes';
$data['p_id'] = $p_id;
echo json_encode($data);	
}
else
{	
$new_post = array(
'post_title' => $post_title,
'post_content' => $post_content,
'post_status' => 'pending',
'post_name' => 'pending',
'post_type' => 'listing'
);
$p_id = wp_insert_post($new_post);
if($not_avalabl)
{
	$not_avalabl_dates = json_encode($not_avalabl);
	$wpdb->insert($availability_calendar_table, array(
                                'user_id' => $user_id, 
                                'not_available_dates' => $not_avalabl_dates,
								'listing_id' => $p_id,
                                ),array(
                                '%d',
                                '%s',
								'%d') 
        );	
}
add_post_meta($p_id, 'dream_destinations', $dream_destinations);
add_post_meta($p_id, 'describe_yourself', $describe_yourself);
if($p_id)
{
	$data['sucess'] ='Yes';
	$data['p_id'] = $p_id;
	echo json_encode($data);
}

}

	

die;    
}



add_action( 'wp_ajax_nopriv_update_listing_second_step', 'update_listing_second_step' );
add_action( 'wp_ajax_update_listing_second_step', 'update_listing_second_step' );
function update_listing_second_step(){
$p_id = $_REQUEST['data']['edit_post_id'];
$place_type = $_REQUEST['data']['place_type'];
$what_will_guests_have = $_REQUEST['data']['what_will_guests_have'];
$guests_accomodate = $_REQUEST['data']['guests_accomodate'];
$guest_bedrooms = $_REQUEST['data']['guest_bedrooms'];
$beds_use = $_REQUEST['data']['beds_use'];
$gust_bathrooms = $_REQUEST['data']['gust_bathrooms'];
$latitude = $_REQUEST['data']['latitude'];
$longitude = $_REQUEST['data']['longitude'];
$country_name = $_REQUEST['data']['country_name'];
$state_name = $_REQUEST['data']['state_name'];
$amenities = $_REQUEST['data']['amenities'];
$check_out_request = $_REQUEST['data']['check_out_request'];
update_post_meta($p_id, 'place_type', $place_type);
update_post_meta($p_id, 'what_will_guests_have', $what_will_guests_have);
update_post_meta($p_id, 'guests_accomodate', $guests_accomodate);
update_post_meta($p_id, 'guest_bedrooms', $guest_bedrooms);
update_post_meta($p_id, 'beds_use', $beds_use);
update_post_meta($p_id, 'gust_bathrooms', $gust_bathrooms);
update_post_meta($p_id, 'latitude', $latitude);
update_post_meta($p_id, 'longitude', $longitude);
update_post_meta($p_id, 'country_name', $country_name);
update_post_meta($p_id, 'state_name', $state_name);
update_post_meta($p_id, 'amenities', $amenities);
update_post_meta($p_id, 'check_out_request', $check_out_request);
$data['sucess'] ='Yes';
$data['p_id'] = $p_id;
echo json_encode($data);
die;	
}


add_action( 'wp_ajax_nopriv_update_listing_third_step', 'update_listing_third_step' );
add_action( 'wp_ajax_update_listing_third_step', 'update_listing_third_step' );
function update_listing_third_step(){
$p_id = $_REQUEST['data']['edit_post_id'];
$describe_pace = $_REQUEST['data']['describe_pace'];
$house_rules = $_REQUEST['data']['house_rules'];
$other_thing = $_REQUEST['data']['other_thing'];
$listing_title = $_REQUEST['data']['listing_title'];
$post_update = array(
    'ID'         => $p_id,
    'post_title' => $listing_title,
	'post_content' => $describe_pace
  );
wp_update_post( $post_update );
update_post_meta($p_id, 'house_rules', $house_rules);
update_post_meta($p_id, 'other_content', $other_thing);
$data['sucess'] ='Yes';
$data['p_id'] = $p_id;
echo json_encode($data);
die;	
}




function get_the_total_post_created_by_user()
{
global $current_user;	
$args = array(
    'post_type' => 'listing',
    'post_author' => $current_user->ID,
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
);
$query = new WP_Query($args);
}

add_action('wp_ajax_cvf_upload_files', 'cvf_upload_files');
add_action('wp_ajax_nopriv_cvf_upload_files', 'cvf_upload_files'); // Allow front-end submission

function cvf_upload_files(){
   
    $parent_post_id = isset( $_POST['post_id'] ) ? $_POST['post_id'] : 0;  // The parent ID of our attachments
    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg"); // Supported file types
    $max_file_size = 1024 * 500; // in kb
    $max_image_upload = 10; // Define how many images can be uploaded to the current post
    $wp_upload_dir = wp_upload_dir();
    $path = $wp_upload_dir['path'] . '/';
    $count = 0;

    $attachments = get_posts( array(
        'post_type'         => 'attachment',
        'posts_per_page'    => -1,
        'post_parent'       => $parent_post_id,
        'exclude'           => get_post_thumbnail_id() // Exclude post thumbnail to the attachment count
    ) );

    // Image upload handler
    if( $_SERVER['REQUEST_METHOD'] == "POST" ){
       
        // Check if user is trying to upload more than the allowed number of images for the current post
        if( ( count( $attachments ) + count( $_FILES['files']['name'] ) ) > $max_image_upload ) {
            $upload_message[] = "Sorry you can only upload " . $max_image_upload . " images for each Ad";
        } else {
           
            foreach ( $_FILES['files']['name'] as $f => $name ) {
                $extension = pathinfo( $name, PATHINFO_EXTENSION );
                // Generate a randon code for each file name
                $new_filename = cvf_td_generate_random_code( 20 )  . '.' . $extension;
               
                if ( $_FILES['files']['error'][$f] == 4 ) {
                    continue;
                }
               
                if ( $_FILES['files']['error'][$f] == 0 ) {
                    // Check if image size is larger than the allowed file size
                    if ( $_FILES['files']['size'][$f] > $max_file_size ) {
                        $upload_message[] = "$name is too large!.";
                        continue;
                   
                    // Check if the file being uploaded is in the allowed file types
                    } elseif( ! in_array( strtolower( $extension ), $valid_formats ) ){
                        $upload_message[] = "$name is not a valid format";
                        continue;
                   
                    } else{
                        // If no errors, upload the file...
                        if( move_uploaded_file( $_FILES["files"]["tmp_name"][$f], $path.$new_filename ) ) {
                           
                            $count++;

                            $filename = $path.$new_filename;
                            $filetype = wp_check_filetype( basename( $filename ), null );
                            $wp_upload_dir = wp_upload_dir();
                            $attachment = array(
                                'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
                                'post_mime_type' => $filetype['type'],
                                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                                'post_content'   => '',
                                'post_status'    => 'inherit'
                            );
                            // Insert attachment to the database
                            $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

                            require_once( ABSPATH . 'wp-admin/includes/image.php' );
                           
                            // Generate meta data
                            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                            wp_update_attachment_metadata( $attach_id, $attach_data );
                           
                        }
                    }
                }
            }
        }
    }
    // Loop through each error then output it to the screen
    if ( isset( $upload_message ) ) :
        foreach ( $upload_message as $msg ){       
            printf( __('<p class="bg-danger">%s</p>', 'wp-trade'), $msg );
        }
    endif;
   
    // If no error, show success message
    if( $count != 0 ){
        printf( __('<p class = "bg-success">%d files added successfully!</p>', 'wp-trade'), $count );  
    }
   
    exit();
}

// Random code generator used for file names.
function cvf_td_generate_random_code($length=10) {
 
   $string = '';
   $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";
 
   for ($p = 0; $p < $length; $p++) {
       $string .= $characters[mt_rand(0, strlen($characters)-1)];
   }
 
   return $string;
 
}




add_action( 'wp_ajax_nopriv_delete_post_attachment', 'delete_post_attachment' );
add_action( 'wp_ajax_delete_post_attachment', 'delete_post_attachment' );
function delete_post_attachment(){
$attachment_id = $_REQUEST['data']['attachment_id'];
wp_delete_attachment( $attachment_id, true );
$data['sucess'] ='Yes';
echo json_encode($data);
die;	
}
function get_not_avalabl_dates($p_id)
{
	global $wpdb;
	$availability_calendar_table = $wpdb->prefix . "availability_calendar";
	$not_avilable_dates = $wpdb->get_results("SELECT * FROM `".$availability_calendar_table."` WHERE (listing_id = '". $p_id ."')");
	return $not_avilable_dates[0];
}



/**************************upload user profile image*************************/
add_action( 'wp_ajax_nopriv_dg_file_upload_handler', 'dg_file_upload_handler' );
add_action( 'wp_ajax_dg_file_upload_handler', 'dg_file_upload_handler' );
function dg_file_upload_handler()
{
    //Get the file
    $_FILES[$f] = $_FILES[0];
    
    $user = new WP_User(get_current_user_id());
    $json['status'] = 'error';
  
    //Check if the file is available && the user is logged in
    if (!empty($_FILES[$f]) && $user->ID > 0) {
      
        $json = array();
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');
       
        //Handle the media upload using WordPress helper functions
        $attachment_id = media_handle_upload($f, 0);
        $json['aid']   = $attachment_id;
        print_r($attachment_id);
        //If error
        if (is_wp_error($attachment_id)) {
            $json['error'] = "Error.";
        } else {
            //delete current
            $profile_image = get_user_meta($user->ID, 'profile_image', true);
            if ($profile_image) {
                $profile_image = json_decode($profile_image);
                if (isset($profile_image->attachment_id)) {
                    wp_delete_attachment($profile_image->attachment_id, true);
                }
            }
            
            //Generate attachment in the media library
            $attachment_file_path = get_attached_file($attachment_id);
            $data                 = wp_generate_attachment_metadata($attachment_id, $attachment_file_path);
            
            //Get the attachment entry in media library
            $image_full_attributes  = wp_get_attachment_image_src($attachment_id, 'full');
            $image_thumb_attributes = wp_get_attachment_image_src($attachment_id, 'smallthumb');
            
            $arr = array(
                'attachment_id' => $attachment_id,
                'url' => $image_full_attributes[0],
                'thumb' => $image_thumb_attributes[0]
            );
            
            //Save the image in the user metadata
            update_user_meta($user->ID, 'profile_image', json_encode($arr));
            
            $json['src']    = $arr['thumb'];
            $json['status'] = 'ok';
        }
    }
    //Output the json
    die(json_encode($json));
}


add_action( 'wp_ajax_nopriv_load_next_month_calendar', 'load_next_month_calendar' );
add_action( 'wp_ajax_load_next_month_calendar', 'load_next_month_calendar' );
function load_next_month_calendar(){
$current_mn = $_REQUEST['data']['current_mn'];
$current_list = $_REQUEST['data']['current_list'];	
$not_avalabl = get_not_avalabl_dates($current_list);
$not_avalabl_dates = json_decode($not_avalabl->not_available_dates);
$year=2020; // change this to another year 2017 or  2018 or 2019 Or ...
if(strlen($year)!= 4){
$year=date('Y'); // Current Year is taken if year is not set in above line. 
}
$row=3;  //to set the number of rows and columns in yearly calendar ( 1 to 12 )
///// No Edit below //////
$row_no=0; // 
echo "<table class='main'>"; // Outer table 
////// Starting of for loop/// 
///  Creating calendars for each month by looping 12 times ///
if($current_mn==11)
{
	$current_month =$current_mn;
}
elseif($current_mn==12)
{
$current_month=1;	
}
else{
$current_month = $current_mn+1;	
}	

$next_mont = $current_month+1;
for($m=$current_month;$m<=$next_mont;$m++){
$month =date($m);  // Month 
$dateObject = DateTime::createFromFormat('!m', $m);
$monthName = $dateObject->format('F'); // Month name to display at top

$d= 2; // To Finds today's date
//$no_of_days = date('t',mktime(0,0,0,$month,1,$year)); //This is to calculate number of days in a month
$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
//echo $j;// Sunday=0 , Saturday =6
//// if starting day of the week is Monday then add following two lines ///
$j=$j-1;  
if($j<0){$j=6;}  // if it is Sunday //
//// end of if starting day of the week is Monday ////


$adj=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$j);  // Blank starting cells of the calendar 
$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
if($blank_at_end >= 7){$blank_at_end = $blank_at_end - 7 ;} 
$adj2=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$blank_at_end); // Blank ending cells of the calendar

/// Starting of top line showing year and month to select ///////////////
if(($row_no % $row)== 0){
echo "</tr><tr>";
}

if($m==$next_mont)
{	
echo "<td><table class='main'><td colspan=8 align=center>$monthName $year <span current_list='".$current_list."' current_mn='".$next_mont."' class='next_mn'> > </span></td><td align='center'></td></tr>";
}
else
{
$next_mt = $next_mont-1;
echo "<td><table class='main'><td colspan=8 align=center><span current_list='".$current_list."' current_mn='".$next_mt."' class='prev_mn'> < </span>$monthName $year</td><td align='center'></td></tr>";	
}	
echo "<tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr>";

for($i=1;$i<=$no_of_days;$i++){
$pv="$month"."/"."$i"."/"."$year";

if(in_array($pv, $not_avalabl_dates))
{
	echo $adj."<td class=\"date_td\"><a class=\"cal_a al_selected not_avlable\"  cur_date=\"$pv\">$i</a>";
}
else
{	
echo $adj."<td class=\"date_td\"><a class=\"cal_a\" cur_date=\"$pv\">$i</a>"; // This will display the date inside the calendar cell
}
echo " </td>";
$adj='';
$j ++;
if($j==7){echo "</tr><tr>"; // start a new row
$j=0;}

}
echo $adj2;   // Blank the balance cell of calendar at the end 

echo "</tr></table></td>";

$row_no=$row_no+1;
} // end of for loop for 12 months
echo "</table>";
die;
}


/***********************Load the prev month***********************/
add_action( 'wp_ajax_nopriv_load_prev_month_calendar', 'load_prev_month_calendar' );
add_action( 'wp_ajax_load_prev_month_calendar', 'load_prev_month_calendar' );
function load_prev_month_calendar(){
$current_mn = $_REQUEST['data']['current_mn'];
$current_list = $_REQUEST['data']['current_list'];	
$not_avalabl = get_not_avalabl_dates($current_list);
$not_avalabl_dates = json_decode($not_avalabl->not_available_dates);
$year=2020; // change this to another year 2017 or  2018 or 2019 Or ...
if(strlen($year)!= 4){
$year=date('Y'); // Current Year is taken if year is not set in above line. 
}
$row=3;  //to set the number of rows and columns in yearly calendar ( 1 to 12 )
///// No Edit below //////
$row_no=0; // 
echo "<table class='main'>"; // Outer table 
////// Starting of for loop/// 
///  Creating calendars for each month by looping 12 times ///	

if($current_mn==1)
{
	$current_month =11;
}
elseif($current_mn==2){
$current_month = 1;	
}
else
{
$current_month = $current_mn-2;	
}	

$next_mont = $current_month+1;
for($m=$current_month;$m<=$next_mont;$m++){
$month =date($m);  // Month 
$dateObject = DateTime::createFromFormat('!m', $m);
$monthName = $dateObject->format('F'); // Month name to display at top

$d= 2; // To Finds today's date
//$no_of_days = date('t',mktime(0,0,0,$month,1,$year)); //This is to calculate number of days in a month
$no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month

$j= date('w',mktime(0,0,0,$month,1,$year)); // This will calculate the week day of the first day of the month
//echo $j;// Sunday=0 , Saturday =6
//// if starting day of the week is Monday then add following two lines ///
$j=$j-1;  
if($j<0){$j=6;}  // if it is Sunday //
//// end of if starting day of the week is Monday ////


$adj=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$j);  // Blank starting cells of the calendar 
$blank_at_end=42-$j-$no_of_days; // Days left after the last day of the month
if($blank_at_end >= 7){$blank_at_end = $blank_at_end - 7 ;} 
$adj2=str_repeat("<td bgcolor='#ffff00'>*&nbsp;</td>",$blank_at_end); // Blank ending cells of the calendar

/// Starting of top line showing year and month to select ///////////////
if(($row_no % $row)== 0){
echo "</tr><tr>";
}

if($m==$next_mont)
{	
echo "<td><table class='main'><td colspan=8 align=center>$monthName $year <span current_list='".$current_list."' current_mn='".$next_mont."' class='next_mn'> > </span></td><td align='center'></td></tr>";
}
else
{
$next_mt = $next_mont-1;
echo "<td><table class='main'><td colspan=8 align=center><span current_list='".$current_list."' current_mn='".$next_mt."' class='prev_mn'> < </span>$monthName $year</td><td align='center'></td></tr>";	
}	
echo "<tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr>";

for($i=1;$i<=$no_of_days;$i++){
$pv="$month"."/"."$i"."/"."$year";

if(in_array($pv, $not_avalabl_dates))
{
	echo $adj."<td class=\"date_td\"><a class=\"cal_a al_selected not_avlable\"  cur_date=\"$pv\">$i</a>";
}
else
{	
echo $adj."<td class=\"date_td\"><a class=\"cal_a\" cur_date=\"$pv\">$i</a>"; // This will display the date inside the calendar cell
}
echo " </td>";
$adj='';
$j ++;
if($j==7){echo "</tr><tr>"; // start a new row
$j=0;}

}
echo $adj2;   // Blank the balance cell of calendar at the end 

echo "</tr></table></td>";

$row_no=$row_no+1;
} // end of for loop for 12 months
echo "</table>";
die;
}

/*************send message functionality***************/
add_action( 'wp_ajax_nopriv_send_message', 'send_message' );
add_action( 'wp_ajax_send_message', 'send_message' );
function send_message(){
global $wpdb;
$sender_id = $_REQUEST['data']['sender_id'];
$reciver_id = $_REQUEST['data']['reciver_id'];
$message_body = $_REQUEST['data']['message_body'];
$success = $wpdb->insert("wp_agent_messaging", array(
   "sender_id" => $sender_id,
   "receiver_id" => $reciver_id,
   "message_parent_id" => NULL,
   "message" => $message_body,
   "created_at" => date('Y-m-d H:i:s'),
));
	if($success) {
		$data['sucess'] ='Yes';
		echo json_encode($data);
      } else {
		  $data['sucess'] ='No';
		  echo json_encode($data);
	  }


die;	
}

function get_user_prev_message($sender_id, $reciver_id)
{
global $wpdb;
$messages = $wpdb->get_results("SELECT * FROM wp_agent_messaging WHERE sender_id = ".$sender_id."
   AND receiver_id=".$reciver_id." ORDER BY ID DESC" );
return $messages;

}

function get_current_user_messages($user_id)
{
global $wpdb;
$messages = $wpdb->get_results("SELECT * FROM wp_agent_messaging WHERE receiver_id=".$user_id." GROUP BY `sender_id` ORDER BY ID DESC" );
return $messages;
}


?>
