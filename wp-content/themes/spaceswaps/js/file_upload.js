$(document).ready(function(){
	
	function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('.uploaded_img').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}


	$('.upload').on('change', function(event) {
		files = event.target.files;
		var file_size = $('.upload')[0].files[0].size;
		if(file_size>2097152) {
			$("#upload_image_validation").modal('show');
		}
		else
		{		
		readURL(this);
		
		
		var cont = $(this).attr('data-cont');
		var name = $(this).attr('name');

		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		data.append('partner_info_post_id', $(this).closest('form').find("#partner_info_post_id").val() );
		data.append('type', $(this).data('type'));

		$(cont).html('<img src="/assets/images/preloader.gif" />');

		$.ajax({
			url: myAjax.ajaxurl+'?action=dg_file_upload_handler&fname='+name, // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			
			dataType: 'json',
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				console.log(data);
				if(data.error)
				{
					alert(data.error);
				}
				else
				{
					$(cont).html('<img src="'+data.src+'" style="max-width:100%;" />');
					$('[name='+name+'_aid]').val(data.aid);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				// Handle errors here
				console.log('ERRORS: ' + textStatus);
				alert('ERRORS: ' + textStatus);
				$(cont).html('error');
				// STOP LOADING SPINNER
			}
		});
		}
	});


});