$(document).ready(function(){
	
  $(".hitw").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });	
	
$('body').on('click', '.cal_a', function(){
$(this).addClass('al_selected');
$(this).removeClass('cal_a');	
$('.nt_dates').show();
$(this).addClass('not_avlable');
var date_value = $(this).attr('cur_date');
var date_value_class = $(this).attr('cur_date').replace(/\//g, '');
$('.not_avlable_div').append('<input type="text" class="'+date_value_class+'" name="not_avalabl" value="'+date_value+'">');		
});	

$('body').on('click','.al_selected', function(){
		$(this).addClass('cal_a');
		$(this).removeClass('al_selected');	
		$(this).removeClass('not_avlable');	
		var date_value = $(this).attr('cur_date');
		var date_value_class = $(this).attr('cur_date').replace(/\//g, '');
		$('.'+date_value_class).remove();
	});
	
$('.unavilable_action').on('click', function(){	
var number_mn = $(this).attr("mn_days");
var selected_dates = 0;
$(this).parent().parent().parent().find('.date_td a').each(function(){
	if($(this).hasClass( "al_selected" ))
	{
		selected_dates = selected_dates+1;
	}
});
if(parseInt(selected_dates) == parseInt(number_mn))
{
	$(this).parent().parent().parent().find('.date_td a').trigger('click');
}
else if(parseInt(selected_dates)==0)
{
$(this).parent().parent().parent().find('.date_td a').trigger('click');	
}
else if(parseInt(selected_dates) < parseInt(number_mn))
{
	$(this).parent().parent().parent().find('.date_td a').each(function(){
	if($(this).hasClass( "al_selected" ))
	{
	}
	else{
		$(this).trigger('click');
	}
});
	
}

});	


});


$(window).scroll(function() {
 var sticky = $('.step-nav'),
  scroll = $(window).scrollTop();
 if (scroll >= 180) sticky.addClass('fixed');
 else sticky.removeClass('fixed');
 
 $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:2,
            nav:true            
        }
    }
})
});
$(function() {
	
	/**************************secons step plus minus***************/
	$('.min').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		return false;
	});
	$('.plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});
	/****************First step validtaion*******************/
     $('.first_step').click(function(e){
		$('.loader-outer').show();
        $('#listing_form').validate();
        if ($('#listing_form').valid()) // check if form is valid
        {
			 e.preventDefault(); 
			 var describe_yourself = $('#describe_yourself').val();
			 var dream_destinations = [];
			 var not_avalabl = [];
			 $.each($("input[name='dream_destination']"), function(){
                var  in_val = $.trim($(this).val());
				if(in_val)
				{
					dream_destinations.push($(this).val());	
				}		
            });
			 $.each($("input[name='not_avalabl']"), function(){
                var  in_val = $.trim($(this).val());
				if(in_val)
				{
					not_avalabl.push($(this).val());	
				}
			 });		
			
				$.post(
					myAjax.ajaxurl, 
					{
					'action': 'save_listing_first_step',
					 data: { describe_yourself : describe_yourself, dream_destinations : dream_destinations, not_avalabl:not_avalabl}
					}, 
					function(response){
					    var res = $.parseJSON(response);
							if(res.sucess == 'Yes')
							{
								location.href = "http://115.112.129.194:7171/spaceswaps/add-listing-step-second/";
							}	
					}
				)  
        }
        else 
        {
			$('.loader-outer').hide();
            // just show validation errors, dont post
        }
    });
		
/*************************second step validation*****************/
     $('.second_step').click(function(e){
		 $('.loader-outer').show();
        $('#listing_form_second').validate();
        if ($('#listing_form_second').valid()) // check if form is valid
        {
			  e.preventDefault(); 
			 var amenities = [];
			 $.each($("input[name='amenities']:checked"), function(){
                amenities.push($(this).val());
            });
			  var edit_post_id = $('#edit_post_id').val();		
			  var place_type = $('input[name="place_type"]:checked').val();
			  var what_will_guests_have = $('input[name="what_will_guests_have"]:checked').val();
			  var guests_accomodate = $('#guests_accomodate').val();
			  var guest_bedrooms = $('#guest_bedrooms').val();
			  var beds_use = $('#beds_use').val();
			  var gust_bathrooms = $('#gust_bathrooms').val();
			  var latitude = $('#latitude').val();
			  var longitude = $('#longitude').val();
			  var country_name = $('#country_name').val();
			  var state_name = $('#state_name').val();
			  var check_out_request = $('#check_out_request').val();			  
				$.post(
					myAjax.ajaxurl, 
					{
					'action': 'update_listing_second_step',
					 data: {edit_post_id: edit_post_id, place_type : place_type, what_will_guests_have : what_will_guests_have,guests_accomodate :guests_accomodate, guest_bedrooms:guest_bedrooms , beds_use:beds_use, gust_bathrooms: gust_bathrooms, latitude:latitude, longitude:longitude,country_name:country_name,state_name:state_name,  amenities: amenities, check_out_request:check_out_request }
					}, 
					function(response){
					    var res = $.parseJSON(response);
							if(res.sucess == 'Yes')
							{
								location.href = "http://115.112.129.194:7171/spaceswaps/add-listing-step-third/";
							}	
					}
				)  
        }
        else 
        {
            $('.loader-outer').hide();
        }
    });	
	
/*************************Third step validation*****************/	


$(document).on('click', '.remove_attachment', function(){
	$('.loader-outer').show();
	var attachment_id = $(this).attr('attachment_id');
	
	$.post(
		myAjax.ajaxurl, 
		{
		'action': 'delete_post_attachment',
		 data: {attachment_id: attachment_id }
		}, 
		function(response){
			var res = $.parseJSON(response);
				if(res.sucess == 'Yes')
				{
					$('.loader-outer').hide();
				}	
		}
	) 	
	
	$(this).parent(".pip").remove();
         
});		
	
});


$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter(".file-area");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
  
$('body').on('click', '.next_mn', function(){
	$('.loader-outer').show();
	var current_mn = parseInt($(this).attr('current_mn'));
	var current_list = parseInt($(this).attr('current_list'));
					$.post(
					myAjax.ajaxurl, 
					{
					'action': 'load_next_month_calendar',
					 data: {current_mn: current_mn, current_list:current_list}
					}, 
					function(response){
					$('.calendar.table-responsive').html(response);   	
					$('.loader-outer').hide();		
					}
				)
});



$('body').on('click', '.prev_mn', function(){
	$('.loader-outer').show();
	var current_mn = parseInt($(this).attr('current_mn'));
	var current_list = parseInt($(this).attr('current_list'));
					$.post(
					myAjax.ajaxurl, 
					{
					'action': 'load_prev_month_calendar',
					 data: {current_mn: current_mn, current_list:current_list}
					}, 
					function(response){
					$('.calendar.table-responsive').html(response);   	
					$('.loader-outer').hide();		
					}
				)
});

/*************************send message ****************************/
$('body').on('click','.send_message_button', function(e){
		$('.loader-outer').show();
        $('#send_message_form').validate({
		  rules: {
			message_body: {
			  required: true,
			  minlength: 30
			}
		  }
		});
        if ($('#send_message_form').valid()) // check if form is valid
        {
			 e.preventDefault(); 
			 var sender_id = $('.sender_id').val();
			 var reciver_id = $('.reciver_id').val();
			 var message_body = $('#message_body').val();
				$.post(
					myAjax.ajaxurl, 
					{
					'action': 'send_message',
					 data: { sender_id : sender_id, reciver_id : reciver_id, message_body:message_body}
					}, 
					function(response){
					    var res = $.parseJSON(response);
							if(res.sucess == 'Yes')
							{
									$('.loader-outer').hide();
									$("#message_sent_success").modal('show');
							}	
					}
				)  
        }
        else 
        {
			$('.loader-outer').hide();
            // just show validation errors, dont post
        }
    });
$('body').on('click', '.close_pop_up', function(){
	location.reload();
});	

$('body').on('click', '.swapspaces', function(e){
	e.preventDefault();
});	
	
  
});