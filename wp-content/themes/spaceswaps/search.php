<?php get_header(); ?>
<style>
#mapCanvas {
    width: 100%;
    height: 100%;
}
</style>
<script>    
var citymap = {
	<?php 
		$args = array(
	   'post_type' => 'listing',
	   'meta_query' => array(
		  array(
			 'key' => 'country_name',
			 'value' => $_GET['s'],
		  ),		  
	   )
	 );
	 $i=1;
	$wp_query = new WP_Query($args);
	while( $wp_query->have_posts() ) : $wp_query->the_post();
		$latitude = get_field('latitude', $wp_query->ID);
		$longitude = get_field('longitude', $wp_query->ID);
		$title = get_the_title($wp_query->ID);
		echo 'city'.$i.': {center: {lat: '.$latitude.', lng: '.$longitude.'}, title: "'.$title.'"},';	
		$i++;
		endwhile;
	?>
      };
      function initMap() {
        // Create the map.
        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
          zoom: 10,
          center: {lat: <?php echo $latitude; ?>, lng: <?php echo $longitude; ?>},
          mapTypeId: 'terrain'
        });

		

        // Construct the circle for each value in citymap.
        // Note: We scale the area of the circle based on the population.
        for (var city in citymap) {
          // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#38C0B5',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#38C0B5',
            fillOpacity: 0.35,
            map: map,
            center: citymap[city].center,
			title: citymap[city].title,
            radius: 1000
          });
		  var infoWindow = new google.maps.InfoWindow();
		google.maps.event.addListener(cityCircle, "click", function(e) {

		  infoWindow.setContent(this.title);
		  infoWindow.setPosition(this.getCenter());
		  infoWindow.open(map);
    });  
        }
      }


</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3h7_i65CzE_FA6wVJHoBSY3sg9akiQ-g&callback=initMap">
    </script>





<main role="main">
		<!-- pro-msg-sec start -->
<div class="container-fluid search-mid-sec">
	<div class="row">
		<div class="col-md-12 step-nav">			
			<nav class="navbar navbar-expand-sm">  
			  <ul class="navbar-nav">
				<li class="nav-item">
				  <a class="nav-link" href="#">Dates</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#">Guests</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#">Type of Place</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#">More filters</a>
				</li>					
			  </ul>
			</nav> 
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 pro-left-sec">
		
		<?php 
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
	   'post_type' => 'listing',
	   'paged' => $paged,
	   'posts_per_page' => 6,
	   'meta_query' => array(
		  array(
			 'key' => 'country_name',
			 'value' => $_GET['s'],
		  ),
	   )
	 );
		
		$wp_query = new WP_Query($args);
		if( $wp_query->have_posts() ) {
		  while( $wp_query->have_posts() ) : $wp_query->the_post();
		?>
		
			<div class="row result-rw">
				
				<?php 
				$attachments = get_posts( array(
				'post_type' => 'attachment',
				'posts_per_page' => -1,
				'post_parent' => $wp_query->post->ID
			) );;
				if($attachments)
				{
				?>
				<div class="col-md-5 search-gallery">
					<div id="demo" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ul class="carousel-indicators">
					<?php 
					$i=1;
					foreach($attachments as $attachment)
					{
					?>
						<li data-target="#demo" data-slide-to="<?php echo $i; ?>" <?php if($i==1){ echo 'class="active"'; } ?>></li>
					<?php $i++; } ?>	
					  </ul>
					  
					  <!-- The slideshow -->
					  <div class="carousel-inner">
					  <?php 
					  $k=1;
						foreach($attachments as $attachment)
						{
					  ?>
						<div class="carousel-item <?php if($k==1){ echo 'active'; } ?>">
							<img src="<?php echo $attachment->guid; ?>" alt="" />
						</div>
						<?php $k++; } ?>	
					  </div>
					  
					  <!-- Left and right controls -->
					  <a class="carousel-control-prev" href="#demo" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					  </a>
					  <a class="carousel-control-next" href="#demo" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					  </a>
					</div><!-- carousel slide End -->
				</div><!-- search-gallery end -->
			<?php }
                 else
				 {
					 echo '<div class="col-md-5 search-gallery"><img src="'.get_template_directory_uri().'/image/defult-home.png"></div>';
				 }	
			?>	
				<div class="col-md-7 search-content">
					<h3><a href="<?php echo get_permalink($wp_query->ID); ?>"><?php echo get_the_title($wp_query->ID); ?></a></h3>
					<ul>
						<li><?php echo get_field('guests_accomodate', $wp_query->ID ); ?> Guests</li>
						<li><?php echo get_field('guest_bedrooms', $wp_query->ID ); ?> bedrooms</li>
						<li><?php echo get_field('beds_use', $wp_query->ID ); ?> beds</li>
						<li><?php echo get_field('gust_bathrooms', $wp_query->ID ); ?> bathroom</li>
						<?php 
						$amenities = get_field('amenities', $wp_query->ID );
						foreach( $amenities as $amenit)
						{
							echo '<li>'.$amenit.' - </li>';
						}
						?>
					</ul>
					<div class="col-md-12 seach-profile-img">	
					<?php 
					$post_author_id = get_post_field( 'post_author', $wp_query->post->ID );
					$profile_img	= @json_decode(get_user_meta($post_author_id, 'profile_image', true));
					$profile_img  = !$profile_img ? '' : $profile_img;
					?>
					<?php if($profile_img){ ?> <img class="rounded-circle" src="<?php echo $profile_img->thumb; ?>">
					<?php 
					}
					else
					{	
					?>	
					<img src="<?php echo get_template_directory_uri(); ?>/image/default-profile-img.png" class="rounded-circle" />
					<?php } ?>
						<p><i class="fa fa-star"></i>5.0</p>
					</div>
				</div>
			</div>
		
		
		 <?php	
		  endwhile;
		}
		else
		{
			echo '<div class="row result-rw">';
				echo '<p>No listing found</p>';
			echo '</div>';
		}
		
		?>
		
		<!-- Pagination section -->
		<div class="col-md-12">
		<?php get_template_part('pagination'); ?>
	</div>
			<!-- Pagination section End -->
		</div><!-- pro-left-sec end -->
		<div class="col-md-6 pro-right-sec">
			<div id="mapCanvas"></div>
			
		</div><!-- pro-right-sec end -->
	</div>
</div><!-- search-mid-sec end -->
	</main>

<?php get_footer(); ?>
