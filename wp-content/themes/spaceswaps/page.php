<?php get_header(); ?>
	<main role="main" id="innerPage">
	<div class="container-fluid">
		<h1 class="title"><?php the_title(); ?></h1>
	</div>
	<div class="container innerPageContent" style="max-width:768px;">
		<?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
		
	</main>
<?php get_footer(); ?>
