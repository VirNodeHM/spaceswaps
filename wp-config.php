<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'swapyourplace' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'pass@123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xCC6([cIaVyN~K{6=H5_FX_}aBy$Cu$,Eo)]iVk-R*QS;DehBZ3S,L4:-JhP11GG' );
define( 'SECURE_AUTH_KEY',  'S5r]Ghp9tPsflo[S+N{r/!W#*{@_~>.e>oK=xs?B`7mO77>?mZp}]e*liYM+y*|9' );
define( 'LOGGED_IN_KEY',    '(VKP7|w3w3%kzRTbPs^xP5I>E,,Om*iDCT5O&s_#4<y9f>h5JUxv=#2je6%B,peh' );
define( 'NONCE_KEY',        'VK&[+f-&Ta.vXS%J`]b,fW[#js1ZZ_fa.QwnG XTV=H.3OH:[:A;KAm-L+m$BMB?' );
define( 'AUTH_SALT',        '**1Gqk!f4_Nqo!i86g^jOKSQIqVA<oSN^XGb31]g?4z5?872y<!j f`D/gLz?zxe' );
define( 'SECURE_AUTH_SALT', 'jbQ/?D>#qN:z7TVss&W2E><PK2(L7wbh%=}y/:91WR*8M^CBl4Nivhue%!Uk#mqE' );
define( 'LOGGED_IN_SALT',   'WO5FjN@MVy{0o4s32@5#BS@fxkUZ]UrLmR%tTQVRE-=Btc9~HmqFy?Dv;J?@L/>l' );
define( 'NONCE_SALT',       '1y~>[Cx#qIzId:oM.,^mz$XT15gG$4KpTU?a!gu+TZ|QOP#:2l?57e>;BFPzsO_X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

@ini_set('upload_max_size' , '256M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
